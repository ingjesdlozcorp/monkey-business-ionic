import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss'],
})
export class LeaveComponent implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {}

  quit(){
    this.navCtrl.navigateForward("welcome");
  }
}
