import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-go-home',
  templateUrl: './go-home.component.html',
  styleUrls: ['./go-home.component.scss'],
})
export class GoHomeComponent implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {}

  goHome() { 
    this.navCtrl.navigateForward("home");
  }
}
