import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { element } from 'protractor';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  
  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }
  goChapter() { 
    this.navCtrl.navigateForward("chapters");
  }
}
